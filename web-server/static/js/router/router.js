import routes from './routes.js';
import userService from '../services/userService.js'

$(document).ready(function () {

    const route = getRouteIfLogged();
    window.history.pushState({}, '', route);

    $(document).click(function (event) {
        const target = _.get(event, 'target');

        if (!target.matches('[data-nav="nav"]')) { return; }

        event.preventDefault();
        router();
    });

    window.onpopstate = locationHandler;
    window.route = router;

    locationHandler();
});


function getRouteIfLogged() {
    if (userService.isLoggedIn()) {
        return '/';
    }

    return '/login';
}


function router (event) {
    const getEvent = event || _.get(window, 'event');
    const target = _.get(getEvent, 'target');
    const href = _.get(target, 'href');
    const isLoggedIn = userService.isLoggedIn();
    const path = $(target).attr('href');
    const route = getRoute(path);
    const secure = _.get(route, 'secure');

    if (secure === true && !isLoggedIn) { return; }

    window.history.pushState({}, "", href);
    locationHandler();
};

async function locationHandler () {
    const pathName = _.get(window, 'location.pathname');
    const route = getRoute(pathName);
    const template = _.get(route, 'template');

    const page =  
        await fetch(template)
            .then((response) => {
                return response.text();
            });

    addPageInContent({ page, route });
};

function setScript (scriptPath) {
    const script = document.createElement('script');
    const includedScripts = $('head > script');

    _.map(includedScripts, (includedScript) => {
        $(includedScript).remove();
    });

    if (_.isEmpty(scriptPath)) { return; }

    script.type = "module";
    script.src = scriptPath;
    $('head').append(script);
};

function getRoute(path) {
    if (_.isEmpty(path)) {
        return '/';
    }

    return _.has(routes, path) ? _.get(routes, path) : _.get(routes, 404);
};

function addPageInContent({ page, route }) {
    const title = _.get(route, 'title');
    const description = _.get(route, 'description');
    const script = _.get(route, 'script');

    $('#content').html(page);
    $(document).attr('title', title);
    $('meta[name="description"]').attr('content', description);
    setScript(script);
};

export default function redirectUrl(url) {
    window.history.pushState({}, "", url);
    locationHandler();
}