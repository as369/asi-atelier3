import API from "../utils/axios.js";

const rootPath = '/api/room-microservice';

export default {
    async getRooms() {
        const response = await API.get(`${rootPath}/public/rooms`);
        const rooms = _.get(response, 'data');
        return rooms;
    },
    async addRoom({ user, name }) {
        const idUser = _.get(user, 'id_user');
        const response = await API.post(`${rootPath}/admin/rooms`, { idCreator: idUser, name });
        const room = _.get(response, 'data');
        return room;
    },
    async removeRoom({ room }) {
        const idRoom = _.get(room, 'id');
        const idCreator = _.get(room, 'creator');
        const idVisitor = _.get(room, 'visitor');
        const name = _.get(room, 'name');
        const deletedRoom = {
            id_room: idRoom,
            idCreator,
            idVisitor,
            name,
            hasWon: true,
        }
        await API.post(`${rootPath}/admin/rooms`, deletedRoom);
    },
}