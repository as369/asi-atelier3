import userService from "./userService.js"
import API from "../utils/axios.js";

const rootPathCardInstance = '/api/card-microservice/public/cardInstances';
const rootPathTransaction = '/api/transaction-microservice/admin/transactions';

function createBodyRequest({ idCardInstance, user, id_transactionType }) {
    return {
        "idUser": user.id_user,
        "idCardInstance": idCardInstance,
        "date": "2007-12-03T10:15:30",
        "price": 1000.00,
        "transactionTypeDTO":{
            "id_transactionType": id_transactionType
        }
    }
}

export default {
    async buyCard({ idCardInstance }) {
        const user = await userService.getUser({ token: document.cookie });
        const bodyRequest = createBodyRequest({ idCardInstance, user , id_transactionType: 1});
        const response = await API.post(rootPathTransaction, bodyRequest);
        return response
    },
    async sellCard({ idCardInstance }) {
        const user = await userService.getUser({ token: document.cookie });
        const bodyRequest = createBodyRequest({ idCardInstance, user , id_transactionType: 2});
        const response = await API.post(rootPathTransaction, bodyRequest);
        return response
    },
    async getCardsToSell() {
        const path = `${rootPathCardInstance}/sale`
        return await API.get(path);
    }
}