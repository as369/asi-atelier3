import userService from "./userService.js";
import API from "../utils/axios.js";

const rootPath = '/api/card-microservice/public/cards';
const familyPath = '/api/card-microservice/public/families';
const cardInstancePath = "/api/card-microservice/secure/cardInstances"

export default {
    async getCardUser(idUser) {
        if (idUser === undefined) {
            idUser = userService.get_user().id_user
        }
        const response = card_list // await fetch('api/card/user/' + idUser)
        return response;
    },
    async getAllCard() {
        return await API.get(rootPath);
    },
    async getAllFamilies() {
        return API.get(familyPath);
    },
    async getCardFamily({ id }) {
        return API.get(`${familyPath}/${id}`);
    },
    async getCardById({ id }) {
        return API.get(`${rootPath}/${id}`);
    },
    async getCardsForUser({ user }) {
        try {
            const idUser = _.get(user, 'id_user');
            return await API.get(`${cardInstancePath}/user/${idUser}`);
        } catch (errors) {
            throw new Error(errors);
        }
    }
}