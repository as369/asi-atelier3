import HTMLBindableElement from  "../components/abstract/HTMLBindableElement.js";
import playService from '../services/playService.js';
import userService from "../services/userService.js";
import { formatRoom } from '../components/room.functions.js';
import InputCustom from "../components/input.js";


class PlayView extends HTMLBindableElement {
	constructor() {
		super();
        this.nameInput = new InputCustom("text", "name", "Room name");
	}

    async getRooms() {
        return await playService.getRooms();
    }

    async addDataToList({ data }) {
        if (_.isEmpty(data)) { return; }

        const tableContent = $('#tableContent');
        tableContent.empty();

        _.map(data, (room) => {
            const container = this.querySelector('#tableContent');
            const formatedRoom = formatRoom({ room });
            const { id, name, won } = formatedRoom;

            if (won) { return; }
            
            let clone = $('#row').html()

            const newContent = clone
                .replace(/{{id}}/g, id)
                .replace(/{{name}}/g, _.isEmpty(name) ? 'Default' : name)
            clone = newContent

            $('#tableContent').append(clone);
            const triggerDelete = container.lastElementChild.querySelector("#trigger_delete");
            triggerDelete.addEventListener('click', () => { this.onButtonDeleteClick({ room: formatedRoom }) });
        });

    }
	

	async render() {
        const user = await userService.getUser({ token: document.cookie });
        this.rooms = await this.getRooms();

		this.innerHTML = `
        <div id="container" class="ui grid vertical segment">
		    <table class="ui fixed selectable single line celled table" id="cardListId">
                <thead>
                    <tr>
                        <th class="three wide">id</th>
                        <th class="three wide">Name</th>
                        <th class="three wide">Join</th>
                        <th class="three wide">Delete</th>
                    </tr>
                </thead>
                <tbody id="tableContent">
                    
                </tbody>
                <template id="row">
                    <tr>
                        <td>
                        <span>{{id}}</span>
                        </td>
                        <td>
                            <span>{{name}}</span>
                        </td>
                        <td>
                            <a data-nav="nav" href="/room" class="visible content">
                                <i class="play icon" data-nav="nav" href="/room"></i>
                            </a>
                        </td>
                        <td>
                            <div id="trigger_delete" class="ui vertical animated button trigger" tabindex="0">
                                <div class="visible content">
                                    <i class="close icon"></i>
                                </div>
                            </div>
                        </td>
                    </tr>
                </template>
            </table>
            <div class="ui grid">
                <div id="add-room-container" class="ui left icon input">
                    <i class="info icon"></i>
                </div>
                <div class="ui teal submit button" onclick="${this.bind(() => this.addRoom({ user }), "addRoom")}">Add room</div>
            </div>
        </div>
		`;
        $("#add-room-container").append(this.nameInput);
        this.addDataToList({ data: this.rooms });
	}

    onButtonClick({ room }) {
        const idRoom = _.get(room, 'id');
    }

    async onButtonDeleteClick({ room }) {
        await playService.removeRoom({ room });
        this.render();
    }

    async addRoom({ user }) {
        const idCreator = _.get(user, 'id_user');
        const hasAlreadyCreated = _.some(this.rooms, { idCreator, hasWon: false });

        if (hasAlreadyCreated) { return; }

        const name = _.isEmpty(this.nameInput.getValue()) ? 'Default' : this.nameInput.getValue();
        const room = await playService.addRoom({ user, name });
        this.rooms.push(room)
        this.addDataToList({ data: this.rooms });
    }

}

customElements.define('play-view', PlayView);