import CardList from "../components/cardList.js"
import Card from "../components/card.js"
import SaleService from "../services/saleService.js";
import HTMLBindableElement from  "../components/abstract/HTMLBindableElement.js";
import cardService from "../services/cardService.js";
import userService from "../services/userService.js";

class SaleView extends HTMLBindableElement {
	constructor() {
		super();
	}
	
	async getCardsForUser({ user }) {
		const response = await cardService.getCardsForUser({ user });
		const cardsForUser = _.get(response, 'data');
		return cardsForUser;
	}
	
	async render() {
		const user = await userService.getUser({ token: document.cookie });
		const cardsForUser = await this.getCardsForUser({ user });
		this.cards = cardsForUser;
		
		this.innerHTML = `
		<div id="container" class="ui grid vertical segment">
			<div id="cardListContainer" class="twelve wide column"></div>
			<div id="cardContainer" class="four wide column"></div>
		</div>
		`;

		this.cardList = new CardList(this.cards, 'sell', (card) => this.selectCard({ card }));
		$("#cardListContainer").append(this.cardList);
		this.card = new Card({}, 'sell');
		this.card.bindOnButtonClick(() => this.sell())
		$("#cardContainer").append(this.card);
	}
	
	selectCard({ card }) {
		this.card.setCard({ card })
	}
	
	async sell() {
		try {
			const idCardInstance = _.get(this.card, 'card.id_cardInstance');
			await SaleService.sellCard({ idCardInstance });
			this.render();
			window.Components.header.render();
		} catch (errors) {
			throw new Error(errors);
		} 
	}
}

customElements.define('sell-view', SaleView);