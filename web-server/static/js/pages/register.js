import InputCustom from "../components/input.js";
import userService from "../services/userService.js";
import HTMLBindableElement from "../components/abstract/HTMLBindableElement.js";
import redirectUrl from '../router/router.js'

class RegisterForm extends HTMLBindableElement {
    constructor() {
        super();
        this.nameInput = new InputCustom("text", "name", "Name");
        this.surnameInput = new InputCustom("text", "surname", "Surname");
        this.emailInput = new InputCustom("email", "email", "E-mail address");
        this.passwordInput = new InputCustom("password", "password", "Password");
    }
    
    render() {
        this.innerHTML = `
            <form class="ui large form">
                <div class="ui stacked segment">
                    <div class="field">
                        <div id="nameContainer" class="ui left icon input">
                            <i class="user icon"></i>
                        </div>
                    </div>
                    <div class="field">
                        <div id="surnameContainer" class="ui left icon input">
                            <i class="user icon"></i>
                        </div>
                    </div>
                    <div class="field">
                        <div id="emailContainer" class="ui left icon input">
                            <i class="user icon"></i>
                        </div>
                    </div>
                    <div class="field">
                        <div id="passwordContainer" class="ui left icon input">
                            <i class="lock icon"></i>
                        </div>
                    </div>
                    <div class="ui fluid large teal submit button" onclick="${this.bind(() => this.submit(), "create")}">Create</div>
                </div>
            </form>
            <div id="error" class="ui error message hidden"></div>
        `;
        
        $("#nameContainer").append(this.nameInput);
        $("#surnameContainer").append(this.surnameInput);
        $("#emailContainer").append(this.emailInput);
        $("#passwordContainer").append(this.passwordInput);
    }
    
    setError(errorTxt) {
        $("#error").removeClass('hidden');
        $("#error").addClass('show');
        $("#error").html(errorTxt);
    }
    
    async submit() {
        const name = this.nameInput.getValue();
        const surname = this.surnameInput.getValue();
        const mail = this.emailInput.getValue();
        const password = this.passwordInput.getValue();
        const isAdmin = false;
        const money = 0;

        if (_.isEmpty(name) || _.isEmpty(surname) || _.isEmpty(mail) || _.isEmpty(password)) {
            this.setError("Fields must be completed");
            return;
        }

        await userService.register({ name, surname, mail, password, isAdmin, money });

        if (!userService.isLoggedIn()) {
            this.setError("An error occurred during the connection");
            return;
        }

        redirectUrl('/');
    }
}

customElements.define('register-form', RegisterForm);
