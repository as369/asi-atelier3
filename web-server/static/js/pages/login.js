import InputCustom from "../components/input.js";
import userService from "../services/userService.js";
import HTMLBindableElement from "../components/abstract/HTMLBindableElement.js";
import redirectUrl from '../router/router.js'

class LoginForm extends HTMLBindableElement {
    constructor() {
        super();
        this.emailInput = new InputCustom("email", "email", "E-mail address");
        this.passwordInput = new InputCustom("password", "password", "Password");
    }
    
    render() {
        this.innerHTML = `
            <form class="ui large form">
                <div class="ui stacked segment">
                    <div class="field">
                        <div id="emailContainer" class="ui left icon input">
                            <i class="user icon"></i>
                        </div>
                    </div>
                    <div class="field">
                        <div id="passwordContainer" class="ui left icon input">
                            <i class="lock icon"></i>
                        </div>
                    </div>
                    <div class="ui fluid large teal submit button" onclick="${this.bind(() => this.submit(), "login")}">Login</div>
                </div>
            </form>
            <div id="error" class="ui error message hidden"></div>
        `;
        
        $("#emailContainer").append(this.emailInput);
        $("#passwordContainer").append(this.passwordInput);
    }
    
    setError(errorTxt) {
        $("#error").removeClass('hidden');
        $("#error").addClass('show');
        $("#error").html(errorTxt);
    }
    
    async submit() {
        const mail = this.emailInput.getValue();
        const password = this.passwordInput.getValue();

        if (_.isEmpty(mail) || _.isEmpty(password)) {
            this.setError("Fields must be completed");
            return;
        }

        await userService.login({ mail, password });

        if (!userService.isLoggedIn()) {
            this.setError("An error occurred during the connection");
            return;
        }

        window.Components.header.render();

        redirectUrl('/');
    }
}

customElements.define('login-form', LoginForm);
