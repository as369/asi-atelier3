import HTMLBindableElement from  "../components/abstract/HTMLBindableElement.js";
class RoomView extends HTMLBindableElement {
	constructor() {
		super();
	}

	async render() {
		this.innerHTML = `
        <div id="container" class="ui grid vertical segment">
			<h1 class="ui header">You are the creator of the room</h1>
			<a data-nav="nav" href="/play" class="visible content">
				<div data-nav="nav" href="/play" class="ui fluid large teal submit button">Leave room</div>
			</a>
        </div>
		`;
	}
}

customElements.define('room-view', RoomView);