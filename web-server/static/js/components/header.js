import userService from "../services/userService.js";
import HTMLBindableElement from "./abstract/HTMLBindableElement.js";
import redirectUrl from '../router/router.js';

class Header extends HTMLBindableElement {
    constructor() {
        super();
        window.Components = {
            header: this
        };
    }

    async getUser() {
        const isLoggedIn = userService.isLoggedIn();
        if (!isLoggedIn) { return; }

        try {
            const user = await userService.getUser({ token: document.cookie });
            return user;
        } catch(errors) {
            throw new Error(errors);
        }
    }
    
    async render() {
        const user = await this.getUser();
        const money = _.has(user, 'money') ? _.get(user, 'money') : '';
        const surname = _.has(user, 'surname') ? _.get(user, 'surname') : '';
        const name = _.has(user, 'name') ? _.get(user, 'name') : '';

        this.userActions = `
            <div class="ui item">
                <div class="ui grid">
                    <span id="userNameId">${name} ${surname}</span>
                    <div class="sub header">
                        <span>${money}$</span>
                    </div>
                </div>
            </div>
            <div class="ui item" onclick="${this.bind(() => this.clearUser(), "logout")}">Logout</div>
        `;

        this.loginAndRegister = `            
            <a id="login" data-nav="nav" class="ui item" href="/login">Login</a>
            <a id="register" data-nav="nav" class="ui item" href="/register">Register</a>
        `;

        this.innerHTML = `
            <header class="ui secondary menu">
                <a data-nav="nav" class="item" href="/">Home</a>
                <div id="nav-right-menu" class="right menu"></div>
            </header>
        `;

        $('#nav-right-menu').append(this.loginAndRegister);

        if (userService.isLoggedIn()) {
            $('#nav-right-menu').append(this.userActions);
            $('#register').remove();
            $('#login').remove();
        }
    }

    clearUser() {
        userService.logout();
        redirectUrl('/login');
        window.Components.header.render();
    }
}

customElements.define('header-custom', Header);
