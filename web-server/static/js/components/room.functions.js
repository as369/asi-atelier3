export function formatRoom({ room }) {
    if (_.isEmpty(room)) { return {}; }
    return {
        id: _.get(room, 'id_room'),
        creator: _.get(room, 'idCreator'),
        visitor: _.get(room, 'idVisitor'),
        name: _.get(room, 'name'),
        won: _.get(room, 'hasWon'),
    };
}

export default {
    formatRoom
}