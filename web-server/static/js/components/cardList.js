import HTMLBindableElement from "./abstract/HTMLBindableElement.js";
import { formatCard } from './card.functions.js';

class CardList extends HTMLBindableElement {
    constructor(cards, label, callback) {
        super();
        this.cards = cards;
        this.oncardclick = callback;
        this.label = label;
    }

    render() {
        if (_.isEmpty(this.cards)) { return; }
        
        this.innerHTML = `
            <h3 class="ui aligned header">Cards to ${_.toLower(this.label)}</h3>
            <table class="ui fixed selectable single line celled table" id="cardListId">
                <thead>
                    <tr>
                        <th class="three wide">Name</th>
                        <th class="three wide">Description</th>
                        <th>Family</th>
                        <th>HP</th>
                        <th>Energy</th>
                        <th>Defence</th>
                        <th>Attack</th>
                        <th>Price</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="tableContent">
                    <template id="row">
                        <tr>
                            <td>
                                <img class="ui avatar image" src={{img_src}}> <span>{{name}} </span>
                            </td>
                            <td>{{description}}</td>
                            <td>{{family_name}}</td>
                            <td>{{hp}}</td>
                            <td>{{energy}}</td>
                            <td>{{defense}}</td>
                            <td>{{attack}}</td>
                            <td>{{price}}$</td>
                            <td>
                                <div id="trigger" class="ui vertical animated button trigger" tabindex="0">
                                    <div class="hidden content">Sell</div>
                                    <div class="visible content">
                                        <i class="shop icon"></i>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </template>
                </tbody>
            </table>
        `;

        this.addDataToCardList({ data: this.cards });
    }

    addDataToCardList({ data }) {
        _.map(data, (cards) => {
            const card = _.get(cards, 'cardDTO');
            const container = this.querySelector('#tableContent');
            const formatedCard = formatCard({ card });
            const { name, description, attack, defense, energy, hp, family, price } = formatedCard;
            let clone = $('#row').html()

            const newContent = clone
                .replace(/{{family_name}}/g, _.isEmpty(family) ? 'Default' : family)
                .replace(/{{img_src}}/g, 'http://medias.3dvf.com/news/sitegrab/gits2045.jpg')
                .replace(/{{name}}/g, _.isEmpty(name) ? 'Default' : name)
                .replace(/{{description}}/g, _.isEmpty(description) ? 'Default' : description)
                .replace(/{{hp}}/g, !_.isInteger(hp) ? 'Default' : hp)
                .replace(/{{energy}}/g, !_.isInteger(energy) ? 'Default' : energy)
                .replace(/{{attack}}/g, !_.isInteger(attack) ? 'Default' : attack)
                .replace(/{{defense}}/g, !_.isInteger(defense) ? 'Default' : defense)
                .replace(/{{price}}/g, price)
            clone = newContent
            $('#tableContent').append(clone);
            const trigger = container.lastElementChild.querySelector("#trigger");
            trigger.addEventListener('click', () => { this.onButtonClick(cards) });
        });
    }

    onButtonClick(card) {
        this.oncardclick(card);
    }

    setCards(newCards) {
        this.cards = newCards;
        this.render();
    }  
}

customElements.define('card-list', CardList);

export default CardList;    