export function formatCard({ card }) {
    if (_.isEmpty(card)) { return {}; }
    return {
        name: _.get(card, 'nameCard'),
        description: _.get(card, 'descriptionCard'),
        family: _.get(card, 'family.name'),
        attack: _.get(card, 'attackCard'),
        defense: _.get(card, 'defenseCard'),
        energy: _.get(card, 'energyCard'),
        hp: _.get(card, 'hpCard'),
        price: _.get(card, 'priceCard'),
    };
}

export default {
    formatCard
}