const instance = axios.create({
  baseURL: "http://localhost:80",
  timeout: 3000,
});

instance.interceptors.response.use(undefined, function (error) {
  if(error.response.status === 401) {
    return Promise.reject(error);
  }
});

export default instance;
