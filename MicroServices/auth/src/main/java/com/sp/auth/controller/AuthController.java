package com.sp.auth.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sp.common.AuthRest;
import com.sp.common.UserRegisterDTO;
import com.sp.auth.service.AuthService;

@RestController
@RequestMapping("/api/auth-microservice")
public class AuthController implements AuthRest {
	@Autowired
	AuthService authService;
	
	// POST
	@Override
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping(value="/admin/auth/createToken")
	public String createToken(@RequestBody UserRegisterDTO dto) {
		return authService.createToken(dto);
	}
	
	@Override
	@PostMapping(value="/admin/auth/encryptPassword")
	public String encryptPassword(@RequestBody UserRegisterDTO dto) {
		return authService.encryptPassword(dto);
	}
	
	@Override
	@PostMapping(value="/admin/auth/checkPassword")
	public boolean checkPassword(@RequestBody List<UserRegisterDTO> dtoUserFoundAndUserLogging) {
		return authService.checkPassword(dtoUserFoundAndUserLogging);
	}
}
