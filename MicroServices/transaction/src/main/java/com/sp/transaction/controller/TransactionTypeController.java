package com.sp.transaction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sp.transaction.dto.TransactionTypeDTO;
import com.sp.transaction.mapper.TransactionTypeMapper;
import com.sp.transaction.service.TransactionTypeService;

@RequestMapping("/api/transaction-microservice")
@RestController
public class TransactionTypeController {
	@Autowired
	TransactionTypeService transactionTypeService;
	@Autowired
	TransactionTypeMapper transactionTypeMapper;
	
	// GET 
	@GetMapping(value="/public/transactionTypes")
	public List<TransactionTypeDTO> getAllTransactionTypes(){
		return transactionTypeMapper.toDTOList(transactionTypeService.getAllTransactionTypes());
	}
	
	@GetMapping(value="/public/transactionTypes/{id_transaction_type}")
	public TransactionTypeDTO getTransactionTypeById(@PathVariable int id_transaction_type) {
		return transactionTypeMapper.toDTO(transactionTypeService.getTransactionTypeById(id_transaction_type));
	}
	
	//POST
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping(value = "/admin/transactionTypes")
	public void createTransactionType(@RequestBody TransactionTypeDTO dto) {
		transactionTypeService.createTransactionType(transactionTypeMapper.toModel(dto));
	}	
}
