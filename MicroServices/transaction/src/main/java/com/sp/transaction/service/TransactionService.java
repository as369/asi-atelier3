package com.sp.transaction.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.common.CardInstanceDTO;
import com.sp.common.CardInstanceRestClient;
import com.sp.common.UserDTO;
import com.sp.common.UserRestClient;
import com.sp.transaction.entity.Transaction;
import com.sp.transaction.repository.TransactionRepository;

@Service
public class TransactionService {
	private static final UserRestClient userRestClient = new UserRestClient();
	private static final CardInstanceRestClient cardInstanceRestClient = new CardInstanceRestClient();
	@Autowired
	TransactionRepository transactionRepository;
	
	public List<Transaction> getAllTransactions() {
		return transactionRepository.findAll();
	}
	
	public List<Transaction> getTransactionsByIdUser(int id_user) {
		return transactionRepository.findByIdUser(id_user);
	}
	
	public Transaction getTransactionById(Integer id_transaction) {
		return transactionRepository.findById(id_transaction).orElseThrow(() -> new RuntimeException());
	}

	public List<Transaction> getTransactionsByIdCardInstance(int id_cardInstance) {
			return transactionRepository.findByIdCardInstance(id_cardInstance);
	}
	
	public HttpServletResponse createTransaction(Transaction transaction, HttpServletResponse response) {
		CardInstanceDTO cardInstanceToModify = cardInstanceRestClient.getCardInstanceById(transaction.getId_cardInstance());
		UserDTO user = userRestClient.getUserById(transaction.getId_user());
		double cardPrice = cardInstanceToModify.getCardDTO().getPriceCard();
		if (transaction.getTransactionType().getId_transaction_type() == 1) { // Buy
			if (user.getMoney() < cardPrice) {
				try {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Not enough money");
					return response;
				} catch (IOException e) {
					e.printStackTrace();
					return response;
				}
			}else {
				user.setMoney(user.getMoney() - cardPrice);
			}
			cardInstanceToModify.setId_user(transaction.getId_user());
			cardInstanceRestClient.createOrUpdateCardInstance(cardInstanceToModify);
		}else if (transaction.getTransactionType().getId_transaction_type() == 2) { // Sell
			user.setMoney(user.getMoney() + cardPrice);		
			cardInstanceToModify.setId_user(null);
			cardInstanceRestClient.createOrUpdateCardInstance(cardInstanceToModify);
		}
		userRestClient.updateUserMoney(user);
		transactionRepository.save(transaction);
		return null;
	}
}
