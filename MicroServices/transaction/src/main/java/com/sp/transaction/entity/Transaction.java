package com.sp.transaction.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Transaction {
	
	@Id
	@GeneratedValue
	private Integer id_sale;
	@Column(name="price_sale")
	private String price;
	@Column(name="date_sale")
	private LocalDateTime date;
	@Column(name="id_user")
	private Integer id_user;
	@Column(name="id_cardInstance")
	private Integer id_cardInstance;
	
	@ManyToOne
	@JoinColumn(name = "id_transaction_type", foreignKey = @ForeignKey(name = "fk_transaction_type_id_transaction_type"))
	private TransactionType transactionType;
	

	public Transaction() {

	}

	public Integer getId_sale() {
		return id_sale;
	}

	public Transaction setId_sale(Integer id_sale) {
		this.id_sale = id_sale;
		return this;
	}

	public String getPrice() {
		return price;
	}

	public Transaction setPrice(String price) {
		this.price = price;
		return this;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public Transaction setDate(LocalDateTime date) {
		this.date = date;
		return this;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public Transaction setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
		return this;
	}

	public Integer getId_user() {
		return id_user;
	}

	public Transaction setId_user(Integer id_user) {
		this.id_user = id_user;
		return this;
	}

	public Integer getId_cardInstance() {
		return id_cardInstance;
	}

	public Transaction setId_cardInstance(Integer id_cardInstance) {
		this.id_cardInstance = id_cardInstance;
		return this;
	}	
}
