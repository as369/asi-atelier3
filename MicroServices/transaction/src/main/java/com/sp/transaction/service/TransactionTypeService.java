package com.sp.transaction.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.transaction.entity.TransactionType;
import com.sp.transaction.repository.TransactionTypeRepository;

@Service
public class TransactionTypeService {
	@Autowired
	TransactionTypeRepository transactionTypeRepository;
	
	public List<TransactionType> getAllTransactionTypes() {
		return transactionTypeRepository.findAll();
	}
	
	public TransactionType getTransactionTypeById(Integer id_transaction_type) {
		return transactionTypeRepository.findById(id_transaction_type).orElseThrow(() -> new RuntimeException());
	}
	
	public void createTransactionType(TransactionType transactionType) {
		transactionTypeRepository.save(transactionType);
	}
}
