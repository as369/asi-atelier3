package com.sp.transaction.dto;

import java.time.LocalDateTime;

public class TransactionDTO {
	private String price;
	private LocalDateTime date;
	private int idCardInstance;
	private int idUser;
	private TransactionTypeDTO transactionTypeDTO;
	
	public TransactionDTO() {
	}
	
	public String getPrice() {
		return price;
	}
	
	public TransactionDTO setPrice(String price) {
		this.price = price;
		return this;
	}
	
	public LocalDateTime getDate() {
		return date;
	}
	
	public TransactionDTO setDate(LocalDateTime date) {
		this.date = date;
		return this;
	}

	public int getIdCardInstance() {
		return idCardInstance;
	}

	public TransactionDTO setIdCardInstance(int idCardInstance) {
		this.idCardInstance = idCardInstance;
		return this;
	}

	public int getIdUser() {
		return idUser;
	}

	public TransactionDTO setIdUser(int idUser) {
		this.idUser = idUser;
		return this;
	}

	public TransactionTypeDTO getTransactionTypeDTO() {
		return transactionTypeDTO;
	}

	public TransactionDTO setTransactionTypeDTO(TransactionTypeDTO transactionTypeDTO) {
		this.transactionTypeDTO = transactionTypeDTO;
		return this;
	}
}
