package com.sp.transaction.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sp.transaction.dto.TransactionDTO;
import com.sp.transaction.entity.Transaction;

@Component
public class TransactionMapper {
	@Autowired
	TransactionTypeMapper transactionTypeMapper;
	public Transaction toModel(TransactionDTO dto) {
		return new Transaction()
				.setPrice(dto.getPrice())
				.setDate(dto.getDate())
				.setId_cardInstance(dto.getIdCardInstance())
				.setId_user(dto.getIdUser())
				.setTransactionType(transactionTypeMapper.toModel(dto.getTransactionTypeDTO()));
	}
	
	public List<Transaction> toModelList(List<TransactionDTO> listDTO) {
		List<Transaction> toReturn = new ArrayList<Transaction>();
		for(TransactionDTO transactionDTO:listDTO) {
			toReturn.add(toModel(transactionDTO));
		}
		return toReturn;
	}
	
	public TransactionDTO toDTO(Transaction transaction) {
		return new TransactionDTO()
				.setDate(transaction.getDate())
				.setIdCardInstance(transaction.getId_cardInstance())
				.setIdUser(transaction.getId_user())
				.setPrice(transaction.getPrice())
				.setTransactionTypeDTO(transactionTypeMapper.toDTO(transaction.getTransactionType()));
	}	
	
	public List<TransactionDTO> toDTOList(List<Transaction> listTransaction) {
		List<TransactionDTO> toReturn = new ArrayList<TransactionDTO>();
		for(Transaction transaction:listTransaction) {
			toReturn.add(toDTO(transaction));
		}
		return toReturn;
	}
}


