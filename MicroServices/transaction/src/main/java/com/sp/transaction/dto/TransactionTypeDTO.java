package com.sp.transaction.dto;

public class TransactionTypeDTO {
	private Integer id_transactionType;
	private String name;

	public TransactionTypeDTO() {
	}
	
	public String getName() {
		return name;
	}

	public TransactionTypeDTO setName(String name) {
		this.name = name;
		return this;
	}

	public Integer getId_transactionType() {
		return id_transactionType;
	}

	public TransactionTypeDTO setId_transactionType(Integer id_transactionType) {
		this.id_transactionType = id_transactionType;
		return this;
	}
}
