package com.sp.transaction.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sp.transaction.entity.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
	@Query("select u from Transaction u where u.id_user = ?1")
	public List<Transaction> findByIdUser(int id_user);
	  
	@Query("select u from Transaction u where u.id_cardInstance = ?1")
	public List<Transaction> findByIdCardInstance(int id_cardInstance);
}


