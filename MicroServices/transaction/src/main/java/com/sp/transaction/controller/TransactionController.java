package com.sp.transaction.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sp.transaction.dto.TransactionDTO;
import com.sp.transaction.mapper.TransactionMapper;
import com.sp.transaction.service.TransactionService;
import com.sp.transaction.service.TransactionTypeService;

@RequestMapping("/api/transaction-microservice")
@RestController
public class TransactionController {
	@Autowired
	TransactionService transactionService;
	@Autowired
	TransactionTypeService transactionTypeService;
	@Autowired
	TransactionMapper transactionMapper;
	
	// GET 
	@GetMapping(value="/public/transactions")
	public List<TransactionDTO> getAllTransactions(){
		return transactionMapper.toDTOList(transactionService.getAllTransactions());
	}
	
	@GetMapping(value="/public/transactions/{id_transaction}")
	public TransactionDTO getTransactionById(@PathVariable int id_transaction) {
		return transactionMapper.toDTO(transactionService.getTransactionById(id_transaction));
	}
	
	// POST
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping(value="/admin/transactions")
	public void createTransaction(@RequestBody TransactionDTO transactionDTO, HttpServletResponse response) {
		transactionService.createTransaction(transactionMapper.toModel(transactionDTO), response);
	}

	// Exception
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public String onException(Exception e) {
		return e.getMessage();
	}
}