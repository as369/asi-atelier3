package com.sp.transaction.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sp.transaction.dto.TransactionTypeDTO;
import com.sp.transaction.entity.TransactionType;

@Component
public class TransactionTypeMapper {
	public TransactionType toModel(TransactionTypeDTO dto) {
		return new TransactionType()
				.setId_transaction_type(dto.getId_transactionType())
				.setName(dto.getName());
	}
	
	public List<TransactionType> toModelList(List<TransactionTypeDTO> listDTO) {
		List<TransactionType> toReturn = new ArrayList<TransactionType>();
		for(TransactionTypeDTO transactionTypeDTO:listDTO) {
			toReturn.add(toModel(transactionTypeDTO));
		}
		return toReturn;
	}
	
	public TransactionTypeDTO toDTO(TransactionType transactionType) {
		return new TransactionTypeDTO()
				.setId_transactionType(transactionType.getId_transaction_type())
				.setName(transactionType.getName());
	}
	
	public List<TransactionTypeDTO> toDTOList(List<TransactionType> listTransactionType) {
		List<TransactionTypeDTO> toReturn = new ArrayList<TransactionTypeDTO>();
		for(TransactionType transactionType:listTransactionType) {
			toReturn.add(toDTO(transactionType));
		}
		return toReturn;
	}
}
