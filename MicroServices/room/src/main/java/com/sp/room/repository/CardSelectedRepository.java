package com.sp.room.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sp.room.entity.CardSelected;
import com.sp.room.entity.Room;

public interface CardSelectedRepository extends JpaRepository<CardSelected, Integer> {
	public List<CardSelected> findByRoom(Room room);
}
