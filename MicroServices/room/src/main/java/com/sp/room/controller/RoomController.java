package com.sp.room.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sp.room.dto.RoomDTO;
import com.sp.room.mapper.RoomMapper;
import com.sp.room.service.RoomService;

@RequestMapping("/api/room-microservice")
@RestController
public class RoomController {
	@Autowired
	RoomService roomService;
	@Autowired
	RoomMapper roomMapper;
	
	@GetMapping(value="/public/rooms")
	public List<RoomDTO> getAllRooms(){
		return roomMapper.toDTOList(roomService.getAllRooms());
	}
	
	@GetMapping(value="/public/rooms/{id_room}")
	public RoomDTO getRoomById(@PathVariable Integer id_room){
		return roomMapper.toDTO(roomService.getRoomById(id_room));
	}
	
	@GetMapping(value="/secure/rooms/{idCreator}")
	public List<RoomDTO> getRoomsByCreatorId(Integer idCreator){
		return roomMapper.toDTOList(roomService.getRoomsByCreatorId(idCreator));
	}
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping(value="/admin/rooms")
	public RoomDTO createOrUpdateRoom(@RequestBody RoomDTO dto, HttpServletResponse response){
		return roomMapper.toDTO(roomService.createOrUpdateRoom(roomMapper.toModel(dto), response));
	}

}
