package com.sp.room.dto;

public class CardSelectedDTO {
	private Integer id_card_selected;
	private int idCardInstance;
	private RoomDTO roomDTO;
	
	public Integer getId_card_selected() {
		return id_card_selected;
	}
	public CardSelectedDTO setId_card_selected(Integer id_card_selected) {
		this.id_card_selected = id_card_selected;
		return this;
	}
	public int getIdCardInstance() {
		return idCardInstance;
	}
	public CardSelectedDTO setIdCardInstance(int idCardInstance) {
		this.idCardInstance = idCardInstance;
		return this;
	}
	public RoomDTO getRoomDTO() {
		return roomDTO;
	}
	public CardSelectedDTO setRoomDTO(RoomDTO roomDTO) {
		this.roomDTO = roomDTO;
		return this;
	}
}
