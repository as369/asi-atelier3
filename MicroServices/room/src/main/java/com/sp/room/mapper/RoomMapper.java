package com.sp.room.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sp.room.dto.RoomDTO;
import com.sp.room.entity.Room;

@Component
public class RoomMapper {
	public Room toModel(RoomDTO dto) {
		return new Room()
				.setName(dto.getName())
				.setId_room(dto.getId_room())
				.setIdCreator(dto.getIdCreator())
				.setIdVisitor(dto.getIdVisitor())
				.setHasWon(dto.isHasWon());
		}
	
	public List<Room> toModelList(List<RoomDTO> listDTO) {
		List<Room> toReturn = new ArrayList<Room>();
		for(RoomDTO roomDTO:listDTO) {
			toReturn.add(toModel(roomDTO));
		}
		return toReturn;
	}
	
	public RoomDTO toDTO(Room room) {
		return new RoomDTO()
				.setName(room.getName())
				.setId_room(room.getId_room())
				.setIdCreator(room.getIdCreator())
				.setIdVisitor(room.getIdVisitor())
				.setHasWon(room.isHasWon());
	}
	
	public List<RoomDTO> toDTOList(List<Room> listRoom) {
		List<RoomDTO> toReturn = new ArrayList<RoomDTO>();
		for(Room room:listRoom) {
			toReturn.add(toDTO(room));
		}
		return toReturn;
	}
}
