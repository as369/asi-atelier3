package com.sp.room.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sp.room.dto.CardSelectedDTO;
import com.sp.room.mapper.CardSelectedMapper;
import com.sp.room.service.CardSelectedService;
import com.sp.room.service.RoomService;

@RequestMapping("/api/room-microservice")
@RestController
public class CardSelectedController {
	@Autowired 
	RoomService roomService;
	@Autowired
	CardSelectedService cardSelectedService;
	@Autowired
	CardSelectedMapper cardSelectedMapper;
	
	@GetMapping(value="/public/cards-selected")
	public List<CardSelectedDTO> getAllCardsSelected(){
		return cardSelectedMapper.toDTOList(cardSelectedService.getAllCardsSelected());
	}
	
	@GetMapping(value="/public/cards-selected/{id_card_selected}")
	public CardSelectedDTO getCardSelectedById(@PathVariable Integer id_card_selected){
		return cardSelectedMapper.toDTO(cardSelectedService.getCardSelectedById(id_card_selected));
	}
	
	@GetMapping(value="/secure/cards-selected/{id_room}")
	public List<CardSelectedDTO> getCardsSelectedByRoom(@PathVariable Integer id_room){
		return cardSelectedMapper.toDTOList(cardSelectedService.getCardsSelectedByRoom(roomService.getRoomById(id_room)));
	}
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping(value="/admin/cards-selected")
	public void createRoom(@RequestBody CardSelectedDTO dto){
		cardSelectedService.createCardSelected(cardSelectedMapper.toModel(dto));
	}
}
