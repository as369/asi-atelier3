package com.sp.room.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Room {
	@Id
	@GeneratedValue
	private Integer id_room;
	@Column(name="id_creator")
	private int idCreator;
	@Column(name="id_visitor")
	private int idVisitor;
	@Column(name="has_won")
	private boolean hasWon;
	@Column(name="name_room")
	private String name;
	
	public Integer getId_room() {
		return id_room;
	}
	public Room setId_room(Integer id_room) {
		this.id_room = id_room;
		return this;
	}
	public int getIdCreator() {
		return idCreator;
	}
	public Room setIdCreator(int idCreator) {
		this.idCreator = idCreator;
		return this;
	}
	public int getIdVisitor() {
		return idVisitor;
	}
	public Room setIdVisitor(int idVisitor) {
		this.idVisitor = idVisitor;
		return this;
	}
	public String getName() {
		return name;
	}
	public Room setHasWon(boolean hasWon) {
		this.hasWon = hasWon;
		return this;
	}

	public boolean isHasWon() {
		return hasWon;
	}
	public Room setName(String name) {
		this.name = name;
		return this;
	}
}
