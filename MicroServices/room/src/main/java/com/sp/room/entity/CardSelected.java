package com.sp.room.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class CardSelected {
	@Id
	@GeneratedValue
	private Integer id_card_selected;
	@Column(name="id_cardInstance")
	private int idCardInstance;
	
	@ManyToOne
	@JoinColumn(columnDefinition="integer", name = "id_room", foreignKey = @ForeignKey(name = "fk_room_id_room"))
	private Room room;

	public Integer getId_card_selected() {
		return id_card_selected;
	}

	public CardSelected setId_card_selected(Integer id_card_selected) {
		this.id_card_selected = id_card_selected;
		return this;
	}

	public int getIdCardInstance() {
		return idCardInstance;
	}

	public CardSelected setIdCardInstance(int idCardInstance) {
		this.idCardInstance = idCardInstance;
		return this;
	}

	public Room getRoom() {
		return room;
	}

	public CardSelected setRoom(Room room) {
		this.room = room;
		return this;
	}
	
	
}
