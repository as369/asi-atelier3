package com.sp.room.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.room.entity.CardSelected;
import com.sp.room.entity.Room;
import com.sp.room.repository.CardSelectedRepository;

@Service
public class CardSelectedService {
	@Autowired
	CardSelectedRepository cardSelectedRepository;
	
	public List<CardSelected> getAllCardsSelected() {
		return cardSelectedRepository.findAll();
	}
	
	public CardSelected getCardSelectedById(Integer id_card_selected) {
		return cardSelectedRepository.findById(id_card_selected).orElseThrow(() -> new RuntimeException());
	}
	
	public List<CardSelected> getCardsSelectedByRoom(Room room) {
		return cardSelectedRepository.findByRoom(room);
	}
	
	public void createCardSelected(CardSelected cardSelected) {
		cardSelectedRepository.save(cardSelected);
	}
	
}
