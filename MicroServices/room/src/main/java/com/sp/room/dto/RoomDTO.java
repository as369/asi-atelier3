package com.sp.room.dto;

public class RoomDTO {
	private Integer id_room;
	private int idCreator;
	private int idVisitor;
	private boolean hasWon;
	private String name;
	
	public Integer getId_room() {
		return id_room;
	}
	public RoomDTO setId_room(Integer id_room) {
		this.id_room = id_room;
		return this;
	}
	public int getIdCreator() {
		return idCreator;
	}
	public RoomDTO setIdCreator(int idCreator) {
		this.idCreator = idCreator;
		return this;
	}
	public int getIdVisitor() {
		return idVisitor;
	}
	public RoomDTO setIdVisitor(int idVisitor) {
		this.idVisitor = idVisitor;
		return this;
	}
	public boolean isHasWon() {
		return hasWon;
	}
	public RoomDTO setHasWon(boolean hasWon) {
		this.hasWon = hasWon;
		return this;
	}
	public String getName() {
		return name;
	}
	public RoomDTO setName(String name) {
		this.name = name;
		return this;
	}	
	
}
