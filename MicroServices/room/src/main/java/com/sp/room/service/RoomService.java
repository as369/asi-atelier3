package com.sp.room.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.common.CardInstanceDTO;
import com.sp.common.CardInstanceRestClient;
import com.sp.room.entity.Room;
import com.sp.room.repository.RoomRepository;

@Service
public class RoomService {
	private static final 	CardInstanceRestClient cardInstanceRestClient = new CardInstanceRestClient();
	@Autowired
	RoomRepository roomRepository;
	
	public List<Room> getAllRooms(){
		return roomRepository.findAll();
	}
	
	public Room getRoomById(Integer id_room){
		return roomRepository.findById(id_room).orElseThrow(() -> new RuntimeException());
	}
	
	public List<Room> getRoomsByCreatorId(Integer idCreator){
		return roomRepository.findByIdCreator(idCreator);
	}
	
	public Room createOrUpdateRoom(Room room, HttpServletResponse response) { // Check if room exists, if so check if user joining has a card otherwhise throw error
		List<CardInstanceDTO> cardInstanceForUser = cardInstanceRestClient.getCardInstancesByIdUser(room.getIdCreator());
		if (cardInstanceForUser.size() == 0) {
			try {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "No cards");
				return null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return roomRepository.save(room);
	}
}
