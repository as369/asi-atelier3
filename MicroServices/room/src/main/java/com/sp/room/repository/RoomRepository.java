package com.sp.room.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sp.room.entity.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {
	@Query("select u from Room u where u.idCreator = ?1")
	public List<Room> findByIdCreator(Integer idCreator);
}
