package com.sp.room.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sp.room.dto.CardSelectedDTO;
import com.sp.room.entity.CardSelected;

@Component
public class CardSelectedMapper {
	@Autowired
	RoomMapper roomMapper;
	
	public CardSelected toModel(CardSelectedDTO dto) {
		return new CardSelected()
				.setId_card_selected(dto.getId_card_selected())
				.setIdCardInstance(dto.getIdCardInstance())
				.setRoom(roomMapper.toModel(dto.getRoomDTO()));
	}
	
	public List<CardSelected> toModelList(List<CardSelectedDTO> listDTO) {
		List<CardSelected> toReturn = new ArrayList<CardSelected>();
		for(CardSelectedDTO cardSelectedDTO:listDTO) {
			toReturn.add(toModel(cardSelectedDTO));
		}
		return toReturn;
	} 
	
	public CardSelectedDTO toDTO(CardSelected cardSelected) {
		return new CardSelectedDTO()
				.setId_card_selected(cardSelected.getId_card_selected())
				.setIdCardInstance(cardSelected.getIdCardInstance())
				.setRoomDTO(roomMapper.toDTO(cardSelected.getRoom()));
	}
	
	public List<CardSelectedDTO> toDTOList(List<CardSelected> listCardSelected) {
		List<CardSelectedDTO> toReturn = new ArrayList<CardSelectedDTO>();
		for(CardSelected cardSelected:listCardSelected) {
			toReturn.add(toDTO(cardSelected));
		}
		return toReturn;
	} 
}
