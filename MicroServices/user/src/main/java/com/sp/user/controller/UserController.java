package com.sp.user.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sp.common.UserDTO;
import com.sp.common.UserRegisterDTO;
import com.sp.common.UserRest;
import com.sp.user.mapper.UserMapper;
import com.sp.user.mapper.UserRegisterMapper;
import com.sp.user.service.UserService;

@RestController
@RequestMapping("/api/user-microservice")
public class UserController implements UserRest{
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserMapper userMapper;
	
	@Autowired
	UserRegisterMapper userRegisterMapper;
	
	@Override
	@GetMapping("/user/{id_user}")
	public UserDTO getUserById(@PathVariable Integer id_user) {
		return userMapper.toDTO(userService.getUserById(id_user));
	}
	
	@PostMapping("/secure/user/mail")
	public UserDTO getUserByMail(@RequestBody UserRegisterDTO userRegisterDTO) {
		return userMapper.toDTO(userService.getUserByMail(userRegisterDTO.getMail()));
	}
	
	@Override
	@PostMapping("/admin/user/money")
	public void updateUserMoney(@RequestBody UserDTO dto) {
		userService.updateUserMoney(userMapper.toModel(dto));
	}
	
	// Inscription
	@PostMapping("/public/user/signup")
	public void addUser(@RequestBody UserRegisterDTO userRegisterDTO, HttpServletResponse response) {
		userService.addUser(userRegisterMapper.toModel(userRegisterDTO), response);
	}
	// Connexion
	@PostMapping("/public/user/signin")
	public void loginUser(@RequestBody UserRegisterDTO userRegisterDTO, HttpServletResponse response) throws IOException {
		userService.loginUser(userRegisterMapper.toModel(userRegisterDTO), response);
	}
}
