package com.sp.user.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.common.AuthRestClient;
import com.sp.common.CardDTO;
import com.sp.common.CardInstanceDTO;
import com.sp.common.CardInstanceRestClient;
import com.sp.common.CardRestClient;
import com.sp.common.UserRegisterDTO;
import com.sp.user.entity.User;
import com.sp.user.mapper.UserRegisterMapper;
import com.sp.user.repository.UserRepository;

@Service
public class UserService {
	private static final AuthRestClient authRestClient = new AuthRestClient();
	private static final CardRestClient cardRestClient = new CardRestClient();
	private static final CardInstanceRestClient cardInstanceRestClient = new CardInstanceRestClient();
	@Autowired
	UserRegisterMapper userRegisterMapper;
	@Autowired
	UserRepository userRepository;

	public HttpServletResponse addUser(User user, HttpServletResponse response) {	
		String hashedPassword = authRestClient.encryptPassword(userRegisterMapper.toDTO(user));
		user.setPassword(hashedPassword);
		String token = authRestClient.createToken(userRegisterMapper.toDTO(user));
		user.setMoney(1000.00);
		userRepository.save(user);
		try {
			response.getWriter().write(token);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Creating card for the new user
		List<CardDTO> listOfCards = cardRestClient.getAllCards();
		for (int i = 0; i <5; i++) {
			cardInstanceRestClient.createOrUpdateCardInstance(new CardInstanceDTO()
					.setCardDTO(listOfCards.get(new Random().nextInt(listOfCards.size())))
					.setId_user(user.getId_user()));
		}
		return response;
	}
	
	public HttpServletResponse loginUser(User user, HttpServletResponse response) {
		try {
			Optional<User> userFind = userRepository.findByMail(user.getMail());
			// If the mail isn't known display 'mail unkown'
			if (!userFind.isPresent()) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "The mail used doesn't exist");
				return response;
			}
	
			// Password verification
			List<UserRegisterDTO> list = new ArrayList<UserRegisterDTO>();
			list.add(userRegisterMapper.toDTO(userFind.get()));
			list.add(userRegisterMapper.toDTO(user));
			boolean check = authRestClient.checkPassword(list);
			if(!check) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Wrong password for this mail");			
				return response;
			}
			// Creation & Gestion of Token
			String token = authRestClient.createToken(userRegisterMapper.toDTO(user));
			response.getWriter().write(token);
			return response;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return response;
		}

	}
	
	public void updateUserMoney(User user) {
		Optional<User> userFound = userRepository.findById(user.getId_user()); // Not very good, no verification...
		user.setPassword(userFound.get().getPassword());
		userRepository.save(user); 
	}
		
	public User getUserById(Integer idUser) {
		return userRepository.findById(idUser).orElseThrow(() -> new RuntimeException());
	}
	
	public User getUserByMail(String mail) {
		return userRepository.findByMail(mail).orElseThrow(() -> new RuntimeException());
	}

}
