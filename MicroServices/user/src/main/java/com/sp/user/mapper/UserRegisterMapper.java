package com.sp.user.mapper;

import org.springframework.stereotype.Component;

import com.sp.common.UserRegisterDTO;
import com.sp.user.entity.User;

@Component
public class UserRegisterMapper {
	
	public User toModel(UserRegisterDTO userRegisterDTO) {
		User toReturn = new User();
		
		
		toReturn.setIsAdmin(userRegisterDTO.getIsAdmin());
		toReturn.setMoney(userRegisterDTO.getMoney());
		toReturn.setName(userRegisterDTO.getName());
		toReturn.setSurname(userRegisterDTO.getSurname());
		toReturn.setPassword(userRegisterDTO.getPassword());
		toReturn.setMail(userRegisterDTO.getMail());
		
		return toReturn;
		
	}
	
	public UserRegisterDTO toDTO(User user) {
		UserRegisterDTO toReturn = new UserRegisterDTO();
		
		toReturn.setIsAdmin(user.getIsAdmin());
		toReturn.setMoney(user.getMoney());
		toReturn.setName(user.getName());
		toReturn.setSurname(user.getSurname());
		toReturn.setPassword(user.getPassword());
		toReturn.setMail(user.getMail());
		
		return toReturn;
		
	}

}
