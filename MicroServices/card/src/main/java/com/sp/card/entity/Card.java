package com.sp.card.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Card {
	
	@Id
	@GeneratedValue
	private Integer id_card;
	@Column(name="energy_card")
	private int energyCard;
	@Column(name="hp_card")
	private int hpCard;
	@Column(name="attack_card")
	private int attackCard;
	@Column(name="defense_card")
	private int defenseCard;
	@Column(name="name_card")
	private String nameCard;
	@Column(name="description_card")
	private String descriptionCard;
	@Column(name="price_card")
	private double priceCard;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_family", foreignKey = @ForeignKey(name = "fk_family_id_family"))
	private Family family;
	
	public Card() {
	}

	public Integer getId_card() {
		return id_card;
	}

	public Card setId_card(Integer id_card) {
		this.id_card = id_card;
		return this;
	}

	public Family getFamily() {
		return family;
	}

	public Card setFamily(Family family) {
		this.family = family;
		return this;
	}

	public int getEnergyCard() {
		return energyCard;
	}

	public Card setEnergyCard(int energyCard) {
		this.energyCard = energyCard;
		return this;
	}

	public int getHpCard() {
		return hpCard;
	}

	public Card setHpCard(int hpCard) {
		this.hpCard = hpCard;
		return this;
	}

	public int getAttackCard() {
		return attackCard;
	}

	public Card setAttackCard(int attackCard) {
		this.attackCard = attackCard;
		return this;
	}

	public int getDefenseCard() {
		return defenseCard;
	}

	public Card setDefenseCard(int defenseCard) {
		this.defenseCard = defenseCard;
		return this;
	}

	public String getNameCard() {
		return nameCard;
	}

	public Card setNameCard(String nameCard) {
		this.nameCard = nameCard;
		return this;
	}

	public String getDescriptionCard() {
		return descriptionCard;
	}

	public Card setDescriptionCard(String descriptionCard) {
		this.descriptionCard = descriptionCard;
		return this;
	}

	public double getPriceCard() {
		return priceCard;
	}

	public Card setPriceCard(double priceCard) {
		this.priceCard = priceCard;
		return this;
	}
	
	
}