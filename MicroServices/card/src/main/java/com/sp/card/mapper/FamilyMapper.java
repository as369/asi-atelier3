package com.sp.card.mapper;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sp.common.FamilyDTO;
import com.sp.card.entity.Family;

@Component
public class FamilyMapper {
	public Family toModel(FamilyDTO dto) {			
		return new Family()			
				.setId_family(dto.getId_family())
				.setName(dto.getName());
		}
	
	public List<Family> toModelList(List<FamilyDTO> listDTO) {
		List<Family> toReturn = new ArrayList<Family>();
		for(FamilyDTO familyDTO:listDTO) {
			toReturn.add(toModel(familyDTO));
		}
		return toReturn;
	}
	
	public FamilyDTO toDTO(Family family) {
		return new FamilyDTO()
				.setId_family(family.getId_family())
				.setName(family.getName());
	}
	
	public List<FamilyDTO> toDTOList(List<Family> listFamily) {
		List<FamilyDTO> toReturn = new ArrayList<FamilyDTO>();
		for(Family family:listFamily) {
			toReturn.add(toDTO(family));
		}
		return toReturn;
	}
}
