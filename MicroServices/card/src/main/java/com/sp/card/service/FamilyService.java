package com.sp.card.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.card.entity.Family;
import com.sp.card.repository.FamilyRepository;


@Service
public class FamilyService {
	@Autowired
	FamilyRepository familyRepository;
	
	public List<Family> getAllFamilies(){
		return familyRepository.findAll();
	}
	
	public Family getFamilyById(int idFamily){
		return familyRepository.findById(idFamily).orElseThrow(() -> new RuntimeException());
	}
	
	public void createFamily(Family family) {
		familyRepository.save(family);
	}
}
