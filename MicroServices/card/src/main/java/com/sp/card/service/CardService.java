package com.sp.card.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.card.entity.Card;
import com.sp.card.entity.Family;
import com.sp.card.repository.CardRepository;

@Service
public class CardService {
	@Autowired
	CardRepository cardRepository;
	
	public List<Card> getAllCards(){
		return cardRepository.findAll();
	}
	
	public List<Card> getCardsByFamily(Family family){
		return cardRepository.findByFamily(family);
	}
	
	public Card getCardById(Integer idCard) {
		return cardRepository.findById(idCard).orElseThrow(() -> new RuntimeException());
	}
	
	public void createCard(Card card) {
		cardRepository.save(card);
	}
}
