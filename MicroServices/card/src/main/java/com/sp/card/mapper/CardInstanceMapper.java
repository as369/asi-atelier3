package com.sp.card.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sp.common.CardInstanceDTO;
import com.sp.card.entity.CardInstance;
import com.sp.card.service.CardService;

@Component
public class CardInstanceMapper {
	@Autowired
	CardMapper cardMapper;
	
	public CardInstance toModel(CardInstanceDTO dto, CardService cardService) {
		return new CardInstance()
				.setId_instance(dto.getId_cardInstance())
				.setCard(cardMapper.toModel(dto.getCardDTO()))
				.setId_user(dto.getId_user());
		}
	
	public List<CardInstance> toModelList(List<CardInstanceDTO> listDTO, CardService cardService) {
		List<CardInstance> toReturn = new ArrayList<CardInstance>();
		for(CardInstanceDTO cardInstanceDTO:listDTO) {
			toReturn.add(toModel(cardInstanceDTO, cardService));
		}
		return toReturn;
	}
	
	public CardInstanceDTO toDTO(CardInstance cardInstance) {
		return new CardInstanceDTO()
				.setId_cardInstance(cardInstance.getId_instance())
				.setCardDTO(cardMapper.toDTO(cardInstance.getCard()))
				.setId_user(cardInstance.getId_user() == null ? null : cardInstance.getId_user());
		}
	
	public List<CardInstanceDTO> toDTOList(List<CardInstance> listCardInstance) {
		List<CardInstanceDTO> toReturn = new ArrayList<CardInstanceDTO>();
		for(CardInstance cardInstance:listCardInstance) {
			toReturn.add(toDTO(cardInstance));
		}
		return toReturn;
	}
}
