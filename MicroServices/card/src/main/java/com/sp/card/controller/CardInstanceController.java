package com.sp.card.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sp.common.CardInstanceDTO;
import com.sp.common.CardInstanceRest;
import com.sp.card.mapper.CardInstanceMapper;
import com.sp.card.service.CardInstanceService;
import com.sp.card.service.CardService;


@RequestMapping("/api/card-microservice")
@RestController
public class CardInstanceController implements CardInstanceRest {
	@Autowired 
	CardService cardService;
	@Autowired
	CardInstanceService cardInstanceService;
	@Autowired
	CardInstanceMapper cardInstanceMapper;
	
	// GET 
	@Override
	@GetMapping(value="/public/cardInstances")
	public List<CardInstanceDTO> getAllCardInstances(){
		return cardInstanceMapper.toDTOList(cardInstanceService.getAllCardInstances());
	}
	
	@Override
	@GetMapping(value="/public/cardInstances/{id_card_instance}")
	public CardInstanceDTO getCardInstanceById(@PathVariable int id_card_instance) {
		return cardInstanceMapper.toDTO(cardInstanceService.getCardInstanceById(id_card_instance));
	}
	
	@Override
	@GetMapping(value="/secure/cardInstances/user/{id_user}")
	public List<CardInstanceDTO> getCardInstancesByIdUser(@PathVariable int id_user) {
		return cardInstanceMapper.toDTOList(cardInstanceService.getCardInstancesByIdUser(id_user));
	}
	
	@Override
	@GetMapping(value="/public/cardInstances/sale")
	public List<CardInstanceDTO> getCardInstancesForSale() {
		return cardInstanceMapper.toDTOList(cardInstanceService.getCardInstancesForSale());
	}
	
	// POST
	@Override
	@PostMapping(value="/admin/cardInstances")
	public void createOrUpdateCardInstance(@RequestBody CardInstanceDTO dto) {
		cardInstanceService.createCardInstance(cardInstanceMapper.toModel(dto, cardService));
	}
}