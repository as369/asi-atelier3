package com.sp.card.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Family {
	
	@Id
	@GeneratedValue
	private Integer id_family;
	@Column(name="name_family")
	private String name;
	
	public Family() {
		
	}
	
	public Integer getId_family() {
		return id_family;
	}
	public Family setId_family(Integer id_family) {
		this.id_family = id_family;
		return this;
	}

	public String getName() {
		return name;
	}

	public Family setName(String name) {
		this.name = name;
		return this;
	}
}
