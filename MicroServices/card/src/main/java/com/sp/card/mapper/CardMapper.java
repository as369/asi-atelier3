package com.sp.card.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sp.common.CardDTO;
import com.sp.card.entity.Card;

@Component
public class CardMapper {
	@Autowired 
	FamilyMapper familyMapper;
	
	public Card toModel(CardDTO dto) {
		return new Card()
				.setId_card(dto.getId_card())
				.setAttackCard(dto.getAttackCard())
				.setDefenseCard(dto.getDefenseCard())
				.setDescriptionCard(dto.getDescriptionCard())
				.setNameCard(dto.getNameCard())
				.setEnergyCard(dto.getEnergyCard())
				.setHpCard(dto.getHpCard())
				.setFamily(familyMapper.toModel(dto.getFamilyDTO()))
				.setPriceCard(dto.getPriceCard());
		}
	
	public List<Card> toModelList(List<CardDTO> listDTO) {
		List<Card> toReturn = new ArrayList<Card>();
		for(CardDTO cardDTO:listDTO) {
			toReturn.add(toModel(cardDTO));
		}
		return toReturn;
	}
	
	public CardDTO toDTO(Card card) {
		return new CardDTO()
				.setAttackCard(card.getAttackCard())
				.setDefenseCard(card.getDefenseCard())
				.setDescriptionCard(card.getDescriptionCard())
				.setId_card(card.getId_card())
				.setEnergyCard(card.getEnergyCard())
				.setHpCard(card.getHpCard())
				.setFamilyDTO(familyMapper.toDTO(card.getFamily()))
				.setPriceCard(card.getPriceCard());
		}
	
	public List<CardDTO> toDTOList(List<Card> listCard) {
		List<CardDTO> toReturn = new ArrayList<CardDTO>();
		for(Card card:listCard) {
			toReturn.add(toDTO(card));
		}
		return toReturn;
	}
}
