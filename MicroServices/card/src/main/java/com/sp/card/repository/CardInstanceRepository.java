package com.sp.card.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sp.card.entity.CardInstance;

@Repository
public interface CardInstanceRepository extends JpaRepository<CardInstance, Integer>{
	@Query("select u from CardInstance u where u.id_user = ?1")
	public List<CardInstance> findByIdUser(int id_user);

}
