package com.sp.card.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sp.card.entity.Family;

@Repository
public interface FamilyRepository extends JpaRepository<Family, Integer> {

}