package com.sp.card.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class CardInstance {
	@Id
	@GeneratedValue
	private Integer id_instance;
	@Column(name="id_user")
	private Integer id_user;
	
	@ManyToOne
	@JoinColumn(columnDefinition="integer", name = "id_card", foreignKey = @ForeignKey(name = "fk_card_id_card"))
	private Card card;

	public CardInstance() {
		
	}

	public Integer getId_instance() {
		return id_instance;
	}

	public CardInstance setId_instance(Integer id_instance) {
		this.id_instance = id_instance;
		return this;
	}

	public Card getCard() {
		return card;
	}

	public CardInstance setCard(Card card) {
		this.card = card;
		return this;
	}

	public Integer getId_user() {
		return id_user;
	}

	public CardInstance setId_user(Integer id_user) {
		this.id_user = id_user;
		return this;
	}
}
