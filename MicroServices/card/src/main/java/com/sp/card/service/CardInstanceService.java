package com.sp.card.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.card.entity.CardInstance;
import com.sp.card.repository.CardInstanceRepository;

@Service
public class CardInstanceService {
	@Autowired
	CardInstanceRepository cardInstanceRepository;

	public List<CardInstance> getAllCardInstances() {
		return cardInstanceRepository.findAll();
	}
	
	public List<CardInstance> getCardInstancesByIdUser(int id_user) {
		return cardInstanceRepository.findByIdUser(id_user);
	}
	
	public CardInstance getCardInstanceById(Integer idCardInstance) {
		return cardInstanceRepository.findById(idCardInstance).orElseThrow(() -> new RuntimeException());
	}
	
	public List<CardInstance> getCardInstancesForSale() {
		List<CardInstance> toReturn = new ArrayList<CardInstance>();
		List<CardInstance> listAllCardInstance = cardInstanceRepository.findAll();
		for(CardInstance cardInstance:listAllCardInstance) {
			if (cardInstance.getId_user() == null) {
				toReturn.add(cardInstance);
			}
		}
		return toReturn;
	}

	public void createCardInstance(CardInstance cardInstance) {
		cardInstanceRepository.save(cardInstance);
	}
	
	public void updateCardInstance(CardInstance cardInstance) {
		cardInstanceRepository.save(cardInstance);
	}
}