INSERT INTO family (name_family, id_family)
VALUES
  ('TestOne', 1),
  ('TestTwo', 2);

INSERT INTO card (energy_card, attack_card, defense_card, description_card, name_card, hp_card, price_card, id_family, id_card)
VALUES
  (10, 11, 12, 'CardOne', 'Number One', 100, 10.00, 1, 1),
  (20, 21, 22, 'CardTwo', 'Number Two', 200, 20.00, 1, 2),
  (30, 31, 32, 'CardThree', 'Number Three', 300, 30.00, 2, 3),
  (40, 41, 42, 'CardFour', 'Number Four', 400, 40.00, 2, 4),
  (50, 51, 52, 'CardFive', 'Number Five', 500, 50.00, 1, 5),
  (60, 61, 62, 'CardSix', 'Number Six', 600, 60.00, 2, 6);
