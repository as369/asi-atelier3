package com.sp.common;

public class FamilyDTO {
	private Integer id_family;
	private String name;
	
	public FamilyDTO() {
	}
	
	public String getName() {
		return name;
	}
	
	public FamilyDTO setName(String name) {
		this.name = name;
		return this;
	}

	public Integer getId_family() {
		return id_family;
	}

	public FamilyDTO setId_family(Integer id_family) {
		this.id_family = id_family;
		return this;
	}
}