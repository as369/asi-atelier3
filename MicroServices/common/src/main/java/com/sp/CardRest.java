package com.sp.common;

import java.util.List;

public interface CardRest {
	public List<CardDTO> getAllCards();
	public CardDTO getCardbById(Integer id_card);
	public List<CardDTO> getCardsbByIdFamily(Integer id_family);
	public void createCard(CardDTO dto);
}
