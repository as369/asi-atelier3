package com.sp.common;

public class CardInstanceDTO {
	private Integer id_user;
	private Integer id_cardInstance;
	private CardDTO cardDTO;
	
	public Integer getId_user() {
		return id_user;
	}
	
	public CardInstanceDTO setId_user(Integer id_user) {
		this.id_user = id_user;
		return this;
	}
	
	public Integer getId_cardInstance() {
		return id_cardInstance;
	}
	
	public CardInstanceDTO setId_cardInstance(Integer id_cardInstance) {
		this.id_cardInstance = id_cardInstance;
		return this;
	}

	public CardDTO getCardDTO() {
		return cardDTO;
	}

	public CardInstanceDTO setCardDTO(CardDTO cardDTO) {
		this.cardDTO = cardDTO;
		return this;
	}
}
