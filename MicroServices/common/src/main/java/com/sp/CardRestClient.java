package com.sp.common;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class CardRestClient implements CardRest {
	private static final String URL_PUBLIC = "http://reverse-proxy:80/api/card-microservice/public/cards"; 
	//private static final String URL_SECURE = "http://reverse-proxy:80/api/card-microservice/secure/cards";
	private static final String URL_ADMIN = "http://reverse-proxy:80/api/card-microservice/admin/cards";

	public CardRestClient() {
	}

	@Override
	public List<CardDTO> getAllCards() {
		RestTemplate restTemplate = new RestTemplate();
		CardDTO[] response = restTemplate.getForEntity(URL_PUBLIC, CardDTO[].class).getBody();
		return List.of(response);
	}

	@Override
	public CardDTO getCardbById(Integer id_card) {
		RestTemplate restTemplate = new RestTemplate();
		CardDTO res = restTemplate.getForObject(URL_PUBLIC + "/" + id_card, CardDTO.class);
		return res;
	}

	@Override
	public List<CardDTO> getCardsbByIdFamily(Integer id_family) {
		RestTemplate restTemplate = new RestTemplate();
		CardDTO[] res = restTemplate.getForEntity(URL_PUBLIC + "/family/" + id_family, CardDTO[].class).getBody();
		return List.of(res);
	}

	@Override
	public void createCard(CardDTO dto) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForObject(URL_ADMIN, dto, ResponseEntity.class);
	}

}
