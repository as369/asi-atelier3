package com.sp.common;

import java.util.List;

import org.springframework.web.client.RestTemplate;

public class AuthRestClient implements AuthRest {

	private static final String URL_ADMIN = "http://reverse-proxy:80/api/auth-microservice/admin/auth";

	public AuthRestClient() {
	}
	
	@Override
	public String createToken(UserRegisterDTO dto) {
		RestTemplate restTemplate = new RestTemplate();
		String res = restTemplate.postForObject(URL_ADMIN + "/createToken", dto, String.class);
		return res;
	}

	@Override
	public String encryptPassword(UserRegisterDTO dto) {
		RestTemplate restTemplate = new RestTemplate();
		String res = restTemplate.postForObject(URL_ADMIN + "/encryptPassword", dto, String.class);
		return res;
	}

	@Override
	public boolean checkPassword(List<UserRegisterDTO> dtoUserFoundAndUserLogging) {
		RestTemplate restTemplate = new RestTemplate();
		boolean res = restTemplate.postForObject(URL_ADMIN + "/checkPassword", dtoUserFoundAndUserLogging, boolean.class);
		return res;
	}

}
