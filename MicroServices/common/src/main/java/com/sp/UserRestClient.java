package com.sp.common;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class UserRestClient implements UserRest {
	private static final String URL_PUBLIC = "http://reverse-proxy:80/api/user-microservice/user"; 
	//private static final String URL_SECURE = "http://reverse-proxy:80/api/user-microservice/secure/user";
	private static final String URL_ADMIN = "http://reverse-proxy:80/api/user-microservice/admin/user";

	public UserRestClient() {
		
	}

	@Override
	public UserDTO getUserById(Integer id_user) {
		RestTemplate restTemplate = new RestTemplate();
		UserDTO res = restTemplate.getForObject(URL_PUBLIC + "/" + id_user, UserDTO.class);
		return res;
	}

	@Override
	public void updateUserMoney(UserDTO dto) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForObject(URL_ADMIN + "/money", dto, ResponseEntity.class);
		
	}
}
