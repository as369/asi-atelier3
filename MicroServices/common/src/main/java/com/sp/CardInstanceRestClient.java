package com.sp.common;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class CardInstanceRestClient implements CardInstanceRest {
	private static final String URL_PUBLIC = "http://reverse-proxy:80/api/card-microservice/public/cardInstances"; 
	private static final String URL_SECURE = "http://reverse-proxy:80/api/card-microservice/secure/cardInstances";
	private static final String URL_ADMIN = "http://reverse-proxy:80/api/card-microservice/admin/cardInstances";

	public CardInstanceRestClient() {
	}

	@Override
	public List<CardInstanceDTO> getAllCardInstances() {
		RestTemplate restTemplate = new RestTemplate();
		CardInstanceDTO[] res = restTemplate.getForEntity(URL_PUBLIC, CardInstanceDTO[].class).getBody();
		return List.of(res);
	}

	@Override
	public CardInstanceDTO getCardInstanceById(int id_card_instance) {
		RestTemplate restTemplate = new RestTemplate();
		CardInstanceDTO res = restTemplate.getForObject(URL_PUBLIC + "/" + id_card_instance, CardInstanceDTO.class);
		return res;
	}

	@Override
	public List<CardInstanceDTO> getCardInstancesByIdUser(int id_user) {
		RestTemplate restTemplate = new RestTemplate();
		CardInstanceDTO[] res = restTemplate.getForEntity(URL_SECURE + "/user/" + id_user, CardInstanceDTO[].class).getBody();
		return List.of(res);
	}

	@Override
	public List<CardInstanceDTO> getCardInstancesForSale() {
		RestTemplate restTemplate = new RestTemplate();
		CardInstanceDTO[] res = restTemplate.getForEntity(URL_PUBLIC + "/sale", CardInstanceDTO[].class).getBody();
		return List.of(res);
	}

	@Override
	public void createOrUpdateCardInstance(CardInstanceDTO dto) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForObject(URL_ADMIN, dto, ResponseEntity.class);
	}
}
