package com.sp.common;

import java.util.List;

public interface AuthRest {
	public String createToken(UserRegisterDTO dto);
	public String encryptPassword(UserRegisterDTO dto);
	public boolean checkPassword(List<UserRegisterDTO> dtoUserFoundAndUserLogging);
}
