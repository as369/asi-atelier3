package com.sp.common;

public class CardDTO {
	private Integer id_card;
	private int energyCard;
	private int hpCard;
	private int attackCard;
	private int defenseCard;
	private String nameCard;
	private String descriptionCard;
	private FamilyDTO familyDTO;
	private double priceCard;

	public CardDTO() {
	}

	public Integer getId_card() {
		return id_card;
	}

	public CardDTO setId_card(Integer id_card) {
		this.id_card = id_card;
		return this;
	}
	
	public int getEnergyCard() {
		return energyCard;
	}

	public CardDTO setEnergyCard(int energyCard) {
		this.energyCard = energyCard;
		return this;
	}

	public int getHpCard() {
		return hpCard;
	}

	public CardDTO setHpCard(int hpCard) {
		this.hpCard = hpCard;
		return this;
	}

	public int getAttackCard() {
		return attackCard;
	}

	public CardDTO setAttackCard(int attackCard) {
		this.attackCard = attackCard;
		return this;
	}

	public int getDefenseCard() {
		return defenseCard;
	}

	public CardDTO setDefenseCard(int defenseCard) {
		this.defenseCard = defenseCard;
		return this;
	}

	public String getNameCard() {
		return nameCard;
	}

	public CardDTO setNameCard(String nameCard) {
		this.nameCard = nameCard;
		return this;
	}

	public String getDescriptionCard() {
		return descriptionCard;
	}

	public CardDTO setDescriptionCard(String descriptionCard) {
		this.descriptionCard = descriptionCard;
		return this;
	}

	public FamilyDTO getFamilyDTO() {
		return familyDTO;
	}

	public CardDTO setFamilyDTO(FamilyDTO familyDTO) {
		this.familyDTO = familyDTO;
		return this;
	}

	public double getPriceCard() {
		return priceCard;
	}

	public CardDTO setPriceCard(double priceCard) {
		this.priceCard = priceCard;
		return this;
	}
}