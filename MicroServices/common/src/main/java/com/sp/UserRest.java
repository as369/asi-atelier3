package com.sp.common;

public interface UserRest {
	public UserDTO getUserById(Integer id_user);
	public void updateUserMoney(UserDTO dto);
}
