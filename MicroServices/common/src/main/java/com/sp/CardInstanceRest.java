package com.sp.common;

import java.util.List;

public interface CardInstanceRest {
	public List<CardInstanceDTO> getAllCardInstances();	
	public CardInstanceDTO getCardInstanceById(int id_card_instance);
	public List<CardInstanceDTO> getCardInstancesByIdUser(int id_user);
	public List<CardInstanceDTO> getCardInstancesForSale();
	public void createOrUpdateCardInstance(CardInstanceDTO dto);
}
