#!/bin/bash

(cd ./MicroServices && mvn clean package -DskipTests)
docker-compose down
docker-compose up -d --build